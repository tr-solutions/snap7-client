#include "snap7-fake-plc.h"
#include "snap7plc.h"
#include <QTimer>
#include <QMessageBox>
#include <qendian.h>

Snap7fakeplc::Snap7fakeplc(QWidget *parent)
    : QMainWindow(parent), dbReadBytes(22), dbWriteBytes(32)
{
   plc = new Snap7plc();
   ui.setupUi(this);
   QTimer::singleShot(10, this, SLOT(slotInitPlcServer()));
}
void Snap7fakeplc::RunPlcServer(void) {

   dbWriteCopy =  new uint8_t[dbWriteBytes];
   dbReadCopy =  new uint8_t[dbReadBytes];

   memset(dbWriteCopy, 0, dbWriteBytes);
   memset(dbReadCopy, 0, dbReadBytes);

   plc->SetDBReadArea(100, dbReadBytes);
   plc->SetDBWriteArea(200, dbWriteBytes);

   plc->RunServer();

   QTimer::singleShot(1000, this, SLOT(slotUpdateLiveCounterEvent()));
   QTimer::singleShot(100, this, SLOT(slotUpdateReadCopyEvent()));

}
void Snap7fakeplc::slotToggleReady() {

   pDbWriteRawData_t wrTemp;
   wrTemp = (pDbWriteRawData_t)dbWriteCopy;

   if (ui.toggleReady->isChecked()) {
      wrTemp->MachineReadyFlag = 1;
   }
   else
   {
      wrTemp->MachineReadyFlag = 0;
   }

   QTimer::singleShot(1, this, SLOT(slotUpdateViewEvent()));
}
void Snap7fakeplc::slotToggleError() {
   pDbWriteRawData_t wrTemp;
   wrTemp = (pDbWriteRawData_t)dbWriteCopy;

   if (ui.toggleError->isChecked()) {
      wrTemp->MachineErrorFlag = 1;
   }
   else
   {
      wrTemp->MachineErrorFlag = 0;
   }
   QTimer::singleShot(1, this, SLOT(slotUpdateViewEvent()));
}
void Snap7fakeplc::slotToggleCycleRun() {
   pDbWriteRawData_t wrTemp;
   wrTemp = (pDbWriteRawData_t)dbWriteCopy;

   if (ui.toggleCycleRun->isChecked()) {
      wrTemp->MachineCycleRunningFlag = 1;
   }
   else
   {
      wrTemp->MachineCycleRunningFlag = 0;
   }
   QTimer::singleShot(1, this, SLOT(slotUpdateViewEvent()));
}
void Snap7fakeplc::slotToggleCcylePause() {
   pDbWriteRawData_t wrTemp;
   wrTemp = (pDbWriteRawData_t)dbWriteCopy;

   if (ui.toggleCyclePause->isChecked()) {
      wrTemp->MachineCyclePausedFlag = 1;
   }
   else
   {
      wrTemp->MachineCyclePausedFlag = 0;
   }
   QTimer::singleShot(1, this, SLOT(slotUpdateViewEvent()));
}
void Snap7fakeplc::slotToggleCycleStop() {
   pDbWriteRawData_t wrTemp;
   wrTemp = (pDbWriteRawData_t)dbWriteCopy;

   if (ui.toggleCycleStop->isChecked()) {
      wrTemp->MachineCycleStoppedFlag = 1;
   }
   else
   {
      wrTemp->MachineCycleStoppedFlag = 0;
   }
   QTimer::singleShot(1, this, SLOT(slotUpdateViewEvent()));
}
void Snap7fakeplc::slotToggleProductReady() {
   pDbWriteRawData_t wrTemp;
   wrTemp = (pDbWriteRawData_t)dbWriteCopy;

   if (ui.toggleProductReady->isChecked()) {
      wrTemp->ProductReady = 1;
   }
   else
   {
      wrTemp->ProductReady = 0;
   }
   QTimer::singleShot(1, this, SLOT(slotUpdateViewEvent()));
}
void Snap7fakeplc::slotToggleProductError() {
   pDbWriteRawData_t wrTemp;
   wrTemp = (pDbWriteRawData_t)dbWriteCopy;

   if (ui.toggleProductError->isChecked()) {
      wrTemp->ProductError = 1;
   }
   else
   {
      wrTemp->ProductError = 0;
   }
   QTimer::singleShot(1, this, SLOT(slotUpdateViewEvent()));
}
void Snap7fakeplc::slotToggleProductManufacturing() {
   pDbWriteRawData_t wrTemp;
   wrTemp = (pDbWriteRawData_t)dbWriteCopy;

   if (ui.toggleProductManufacturing->isChecked()) {
      wrTemp->ProductManufacturing = 1;
   }
   else
   {
      wrTemp->ProductManufacturing = 0;
   }
}
void Snap7fakeplc::slotToggleProductLoaded() {
   pDbWriteRawData_t wrTemp;
   wrTemp = (pDbWriteRawData_t)dbWriteCopy;

   if (ui.toggleProductLoaded->isChecked()) {
      wrTemp->ProductLoaded = 1;
   }
   else
   {
      wrTemp->ProductLoaded = 0;
   }
   QTimer::singleShot(1, this, SLOT(slotUpdateViewEvent()));
}
void Snap7fakeplc::slotToggleProductInvalid() {
   pDbWriteRawData_t wrTemp;
   wrTemp = (pDbWriteRawData_t)dbWriteCopy;

   if (ui.toggleProductInvalid->isChecked()) {
      wrTemp->ProductInvalid = 1;
   }
   else
   {
      wrTemp->ProductInvalid = 0;
   }
   QTimer::singleShot(1, this, SLOT(slotUpdateViewEvent()));
}
void Snap7fakeplc::slotToggleProductAccepted() {
   pDbWriteRawData_t wrTemp;
   wrTemp = (pDbWriteRawData_t)dbWriteCopy;

   if (ui.toggleProductAccepted->isChecked()) {
      wrTemp->ProductAccepted = 1;
   }
   else
   {
      wrTemp->ProductAccepted = 0;
   }
   QTimer::singleShot(1, this, SLOT(slotUpdateViewEvent()));
}
void Snap7fakeplc::slotToggleDoorsOpened() {
   pDbWriteRawData_t wrTemp;
   wrTemp = (pDbWriteRawData_t)dbWriteCopy;

   if (ui.toggleDoorsOpened->isChecked()) {
      wrTemp->DoorsOpenedFlag = 1;
   }
   else
   {
      wrTemp->DoorsOpenedFlag = 0;
   }
   QTimer::singleShot(1, this, SLOT(slotUpdateViewEvent()));
}
void Snap7fakeplc::slotToggleEStop() {
   pDbWriteRawData_t wrTemp;
   wrTemp = (pDbWriteRawData_t)dbWriteCopy;

   if (ui.toggleEStop->isChecked()) {
      wrTemp->EstopActiveFlag = 1;
   }
   else
   {
      wrTemp->EstopActiveFlag = 0;
   }
   QTimer::singleShot(1, this, SLOT(slotUpdateViewEvent()));
}
void Snap7fakeplc::slotToggleOvertravelRight() {
   pDbWriteRawData_t wrTemp;
   wrTemp = (pDbWriteRawData_t)dbWriteCopy;

   if (ui.toggleOvertravelRight->isChecked()) {
      wrTemp->OvertravelRightFlag = 1;
   }
   else
   {
      wrTemp->OvertravelRightFlag = 0;
   }
   QTimer::singleShot(1, this, SLOT(slotUpdateViewEvent()));
}
void Snap7fakeplc::slotToggleOvertravelLeft() {
   pDbWriteRawData_t wrTemp;
   wrTemp = (pDbWriteRawData_t)dbWriteCopy;

   if (ui.toggleOvertravelLeft->isChecked()) {
      wrTemp->OvertravelLeftFlag = 1;
   }
   else
   {
      wrTemp->OvertravelLeftFlag = 0;
   }
   QTimer::singleShot(1, this, SLOT(slotUpdateViewEvent()));
}
void Snap7fakeplc::slotToggleGrabberError() {
   pDbWriteRawData_t wrTemp;
   wrTemp = (pDbWriteRawData_t)dbWriteCopy;

   if (ui.toggleGrabberError->isChecked()) {
      wrTemp->GrabberErrorFlag = 1;
   }
   else
   {
      wrTemp->GrabberErrorFlag = 0;
  
   }
   QTimer::singleShot(1, this, SLOT(slotUpdateViewEvent()));
}
void Snap7fakeplc::slotToggleTraverseError() {
   pDbWriteRawData_t wrTemp;
   wrTemp = (pDbWriteRawData_t)dbWriteCopy;

   if (ui.toggleTraverseError->isChecked()) {
      wrTemp->TraverseErrorFlag = 1;
   }
   else
   {
      wrTemp->TraverseErrorFlag = 0;
   }
   QTimer::singleShot(1, this, SLOT(slotUpdateViewEvent()));
}
void Snap7fakeplc::slotToggleInverterError() {
   pDbWriteRawData_t wrTemp;
   wrTemp = (pDbWriteRawData_t)dbWriteCopy;

   if (ui.toggleInverterError->isChecked()) {
      wrTemp->InverterErrorFlag = 1;
   }
   else
   {
      wrTemp->InverterErrorFlag = 0;
   }
   QTimer::singleShot(1, this, SLOT(slotUpdateViewEvent()));
}
void Snap7fakeplc::slotToggleReel1Empty() {
   pDbWriteRawData_t wrTemp;
   wrTemp = (pDbWriteRawData_t)dbWriteCopy;

   if (ui.toggleReel1Empty->isChecked()) {
      wrTemp->Reel1Empty = 1;
   }
   else
   {
      wrTemp->Reel1Empty = 0;
   }
}
void Snap7fakeplc::slotToggleReel2Empty() {
   pDbWriteRawData_t wrTemp;
   wrTemp = (pDbWriteRawData_t)dbWriteCopy;

   if (ui.toggleReel2Empty->isChecked()) {
      wrTemp->Reel2Empty = 1;
   }
   else
   {
      wrTemp->Reel2Empty = 0;
   }
   QTimer::singleShot(1, this, SLOT(slotUpdateViewEvent()));
}
void Snap7fakeplc::slotToggleReel3Empty() {
   pDbWriteRawData_t wrTemp;
   wrTemp = (pDbWriteRawData_t)dbWriteCopy;

   if (ui.toggleReel3Empty->isChecked()) {
      wrTemp->Reel3Empty = 1;
   }
   else
   {
      wrTemp->Reel3Empty = 0;
   }
   QTimer::singleShot(1, this, SLOT(slotUpdateViewEvent()));
}
void Snap7fakeplc::slotToggleReel4Empty() {
   pDbWriteRawData_t wrTemp;
   wrTemp = (pDbWriteRawData_t)dbWriteCopy;

   if (ui.toggleReel4Empty->isChecked()) {
      wrTemp->Reel4Empty = 1;
   }
   else
   {
      wrTemp->Reel4Empty = 0;
   }
   QTimer::singleShot(1, this, SLOT(slotUpdateViewEvent()));
}
void Snap7fakeplc::slotToggleReel1Fault() {
   pDbWriteRawData_t wrTemp;
   wrTemp = (pDbWriteRawData_t)dbWriteCopy;

   if (ui.toggleReel1Fault->isChecked()) {
      wrTemp->Reel1Fault = 1;
   }
   else
   {
      wrTemp->Reel1Fault = 0;
   }
   QTimer::singleShot(1, this, SLOT(slotUpdateViewEvent()));
}
void Snap7fakeplc::slotToggleReel2Fault() {
   pDbWriteRawData_t wrTemp;
   wrTemp = (pDbWriteRawData_t)dbWriteCopy;

   if (ui.toggleReel2Fault->isChecked()) {
      wrTemp->Reel2Fault = 1;
   }
   else
   {
      wrTemp->Reel2Fault = 0;
   }
   QTimer::singleShot(1, this, SLOT(slotUpdateViewEvent()));
}
void Snap7fakeplc::slotToggleReel3Fault() {
   pDbWriteRawData_t wrTemp;
   wrTemp = (pDbWriteRawData_t)dbWriteCopy;

   if (ui.toggleReel1Fault->isChecked()) {
      wrTemp->Reel3Fault = 1;
   }
   else
   {
      wrTemp->Reel3Fault = 0;
   }
   QTimer::singleShot(1, this, SLOT(slotUpdateViewEvent()));
}
void Snap7fakeplc::slotToggleReel4Fault() {
   pDbWriteRawData_t wrTemp;
   wrTemp = (pDbWriteRawData_t)dbWriteCopy;

   if (ui.toggleReel4Fault->isChecked()) {
      wrTemp->Reel4Fault = 1;
   }
   else
   {
      wrTemp->Reel4Fault = 0;
   }
   QTimer::singleShot(1, this, SLOT(slotUpdateViewEvent()));
}
void Snap7fakeplc::slotMagicStartChanged(QString input) {

   pDbWriteRawData_t wrTemp;
   uint32_t temporaryUint32;
   wrTemp = (pDbWriteRawData_t)dbWriteCopy;

   bool conversionOk;
   uint32_t value;

   value = input.toULong(&conversionOk, 16);
   qToBigEndian<quint32>(value, &temporaryUint32);

   wrTemp->MagicNumberStart = temporaryUint32;
}
void Snap7fakeplc::slotProductTypeChanged(QString input) {
   pDbWriteRawData_t wrTemp;
   uint32_t temporaryUint16;
   wrTemp = (pDbWriteRawData_t)dbWriteCopy;

   bool conversionOk;
   uint32_t value;

   value = input.toUInt(&conversionOk, 10);
   qToBigEndian<quint16>(value, &temporaryUint16);

   wrTemp->ProductType = temporaryUint16;
}
void Snap7fakeplc::slotProductQtyChanged(QString input) {
   pDbWriteRawData_t wrTemp;
   uint32_t temporaryUint16;
   wrTemp = (pDbWriteRawData_t)dbWriteCopy;

   bool conversionOk;
   uint32_t value;

   value = input.toUInt(&conversionOk, 10);
   qToBigEndian<quint16>(value, &temporaryUint16);

   wrTemp->ProductQty = temporaryUint16;
}
void Snap7fakeplc::slotMagicEndChanged(QString input) {

   pDbWriteRawData_t wrTemp;
   uint32_t temporaryUint32;
   wrTemp = (pDbWriteRawData_t)dbWriteCopy;

   bool conversionOk;
   uint32_t value;

   value = input.toULong(&conversionOk, 16);
   qToBigEndian<quint32>(value, &temporaryUint32);

   wrTemp->MagicNumberEnd = temporaryUint32;

}
void Snap7fakeplc::slotInitPlcServer()
{
   (void)RunPlcServer();
}
void Snap7fakeplc::slotUpdateLiveCounterEvent()
{
   pDbWriteRawData_t wrTemp;
   uint16_t temporaryUint16;
   wrTemp = (pDbWriteRawData_t)dbWriteCopy;

   qToBigEndian<quint16>(wrTemp->LiveCounter, &temporaryUint16);
   ++temporaryUint16;
   qToBigEndian<quint16>(temporaryUint16, &wrTemp->LiveCounter);

   QTimer::singleShot(1000, this, SLOT(slotUpdateLiveCounterEvent()));
   QTimer::singleShot(1, this, SLOT(slotUpdateWriteCopyEvent()));
}
void Snap7fakeplc::slotUpdateWriteCopyEvent()
{
   plc->DBSetData(dbWriteCopy, dbWriteBytes);
   QTimer::singleShot(1, this, SLOT(slotUpdateViewEvent()));
}
void Snap7fakeplc::slotUpdateReadCopyEvent()
{
   plc->DBGetData(dbReadCopy,dbReadBytes);
   QTimer::singleShot(1, this, SLOT(slotUpdateViewEvent())); 
   QTimer::singleShot(100, this, SLOT(slotUpdateReadCopyEvent()));
}
void Snap7fakeplc::slotUpdateViewEvent()
{
   pDbWriteRawData_t wrTemp;
   pDbReadRawData_t rdTemp;
   QString strText;

   uint16_t temporaryUint16;
   uint32_t tMagicNumberStart;
   uint32_t tMagicNumberEnd;
   uint16_t tLiveCounter;
   uint16_t tErpErrorID;
   uint16_t tProductType;
   uint16_t tProductQty;

   wrTemp = (pDbWriteRawData_t)dbWriteCopy;
   rdTemp = (pDbReadRawData_t)dbReadCopy;

   qToBigEndian<quint16>(wrTemp->LiveCounter, &temporaryUint16);
   ui.liveCounterPLC->setText(QString("%1").arg(temporaryUint16, 8, 10, QLatin1Char('0')));


   qToBigEndian<quint32>(rdTemp->MagicNumberStart, &tMagicNumberStart);
   qToBigEndian<quint32>(rdTemp->MagicNumberEnd, &tMagicNumberEnd);
   qToBigEndian<quint16>(rdTemp->LiveCounter, &tLiveCounter);
   qToBigEndian<quint16>(rdTemp->ErpErrorID, &tErpErrorID);

   qToBigEndian<quint16>(rdTemp->ProductType, &tProductType);
   qToBigEndian<quint16>(rdTemp->ProductQty, &tProductQty);

   strText = QString("Magic Start : %1\nLive counter : %2\n\
      ERP OK Flag : %3\n\
      ERP Error Flag : %4\n\
      ERP Error ID : %5\n\
      Request new product : %6\n\
      Request start cycle : %7\n\
      Request pause cycle : %8\n\
      Request stop cycle : %9\n\
      Request error reset : %10\n\
      Product stored flag : %11\n\
      Product type : %12\n\
      Product qty : %13\n\
      Magic End : %14\n").\
      arg(tMagicNumberStart, 8, 16, QLatin1Char('0')).\
      arg(tLiveCounter, 8, 10, QLatin1Char('0')).
      arg(rdTemp->ErpOkFlag, 1, 10, QLatin1Char('0')).
      arg(rdTemp->ErpErrorFlag, 1, 10, QLatin1Char('0')).
      arg(tErpErrorID, 8, 10, QLatin1Char('0')).
      arg(rdTemp->RequestNewProductFlag, 1, 10, QLatin1Char('0')).
      arg(rdTemp->RequestStartCycleFlag, 1, 10, QLatin1Char('0')).
      arg(rdTemp->RequestPauseCycleFlag, 1, 10, QLatin1Char('0')).
      arg(rdTemp->RequestStopCycleFlag, 1, 10, QLatin1Char('0')).
      arg(rdTemp->RequestErrorResetFlag, 1, 10, QLatin1Char('0')).
      arg(rdTemp->ProductStoredFlag, 1, 10, QLatin1Char('0')).
      arg(tProductType, 8, 10, QLatin1Char('0')).
      arg(tProductQty, 8, 10, QLatin1Char('0')).
      arg(tMagicNumberEnd, 8, 16, QLatin1Char('0'));

   ui.erp2plcDataBlockContent->setPlainText(strText);

}
