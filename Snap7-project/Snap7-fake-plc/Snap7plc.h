#pragma once

class TS7Server;

class Snap7plc
{
public:
   Snap7plc();
   ~Snap7plc();
   
   void RunServer(void);

   void SetDBReadArea(int DBNumber, int Size);
   void SetDBWriteArea(int DBNumber, int Size);

   int  DBRead();
   int  DBWrite();

   void DBGetData(void *dbData, int dataSize);
   void DBSetData(void *, int dataSize);

private:
   TS7Server *pS7srv;

   int dbread_no;
   int dbread_size;
   void *dbread_buffer;

   int dbwrite_no;
   int dbwrite_size;
   void *dbwrite_buffer;

};

