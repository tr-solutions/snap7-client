#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_snap7fakeplc.h"

class Snap7plc;
class QTimer;
class QString;

class Snap7fakeplc : public QMainWindow
{
    Q_OBJECT

       // PLC->ERP
    typedef struct {
       uint32_t MagicNumberStart;
       uint16_t LiveCounter;

       union {
          uint16_t StatusBits;
          struct {
             uint16_t MachineReadyFlag : 1;
             uint16_t MachineErrorFlag : 1;
             uint16_t MachineCycleRunningFlag : 1;
             uint16_t MachineCycleStoppedFlag : 1;
             uint16_t MachineCyclePausedFlag : 1;
             uint16_t : 11;
          };
       };

       int16_t MachineCycleStep;

       union {
          uint16_t AlarmsBits;
          struct {
             uint16_t DoorsOpenedFlag : 1;
             uint16_t EstopActiveFlag : 1;
             uint16_t OvertravelLeftFlag : 1;
             uint16_t OvertravelRightFlag : 1;
             uint16_t GrabberErrorFlag : 1;
             uint16_t TraverseErrorFlag : 1;
             uint16_t InverterErrorFlag : 1;
             uint16_t : 9;
          };
       };

       union {
          uint16_t Reel1ErrorBits;
          struct {
             uint16_t Reel1Empty : 1;
             uint16_t Reel1Fault : 1;
             uint16_t : 14;
          };
       };

       union {
          uint16_t Reel2ErrorBits;
          struct {
             uint16_t Reel2Empty : 1;
             uint16_t Reel2Fault : 1;
             uint16_t : 14;
          };
       };

       union {
          uint16_t Reel3ErrorBits;
          struct {
             uint16_t Reel3Empty : 1;
             uint16_t Reel3Fault : 1;
             uint16_t : 14;
          };
       };

       union {
          uint16_t Reel4ErrorBits;
          struct {
             uint16_t Reel4Empty : 1;
             uint16_t Reel4Fault : 1;
             uint16_t : 14;
          };
       };

       union {
          uint16_t ReservedControlBits;
          struct {
             uint16_t ReservedControlBit_0 : 1;
             uint16_t : 15;
          };
       };

       union {
          uint16_t ProductInfoBits;
          struct {
             uint16_t ProductManufacturing : 1;
             uint16_t ProductReady : 1;
             uint16_t ProductError : 1;
             uint16_t ProductAccepted : 1;
             uint16_t ProductInvalid : 1;
             uint16_t ProductLoaded : 1;
             uint16_t : 10;
          };
       };

       uint16_t ProductType;
       uint16_t ProductQty;
       uint32_t MagicNumberEnd;

    } dbWriteRawData_t, *pDbWriteRawData_t;

#pragma pack(push, 1)
    typedef struct {


       uint32_t MagicNumberStart;
       uint16_t LiveCounter;
       union {
          uint16_t StatusBits;
          struct {
             uint16_t ErpOkFlag : 1;
             uint16_t ErpErrorFlag : 1;
             uint16_t : 14;
          };
       };
       uint16_t ErpErrorID;
       union {
          uint16_t ControlBits;
          struct {
             uint16_t RequestNewProductFlag : 1;
             uint16_t RequestStartCycleFlag : 1;
             uint16_t RequestStopCycleFlag : 1;
             uint16_t RequestPauseCycleFlag : 1;
             uint16_t RequestErrorResetFlag : 1;
             uint16_t : 11;
          };
       };
       union {
          uint16_t StatusBits2;
          struct {
             uint16_t ProductStoredFlag : 1;
             uint16_t : 15;
          };
       };

       uint16_t ProductType;
       uint16_t ProductQty;
       uint32_t MagicNumberEnd;


    } dbReadRawData_t, *pDbReadRawData_t;
#pragma pack(pop)

public:
    Snap7fakeplc(QWidget *parent = Q_NULLPTR);
    void RunPlcServer(void);

private slots:
   void slotToggleReady();
   void slotToggleError();
   void slotToggleCycleRun();
   void slotToggleCcylePause();
   void slotToggleCycleStop();
   void slotToggleProductReady();
   void slotToggleProductError();
   void slotToggleProductManufacturing();
   void slotToggleProductLoaded();
   void slotToggleProductInvalid();
   void slotToggleProductAccepted();
   void slotToggleDoorsOpened();
   void slotToggleEStop();
   void slotToggleOvertravelRight();
   void slotToggleOvertravelLeft();
   void slotToggleGrabberError();
   void slotToggleTraverseError();
   void slotToggleInverterError();
   void slotToggleReel1Empty();
   void slotToggleReel2Empty();
   void slotToggleReel3Empty();
   void slotToggleReel4Empty();
   void slotToggleReel1Fault();
   void slotToggleReel2Fault();
   void slotToggleReel3Fault();
   void slotToggleReel4Fault();

   void slotMagicStartChanged(QString);
   void slotProductTypeChanged(QString);
   void slotProductQtyChanged(QString);
   void slotMagicEndChanged(QString);

   void slotUpdateLiveCounterEvent();
   void slotUpdateWriteCopyEvent();
   void slotUpdateReadCopyEvent();
   void slotUpdateViewEvent();
   void slotInitPlcServer();
private:

   const uint16_t dbReadBytes;
   const uint16_t dbWriteBytes;

   uint8_t *dbReadCopy;
   uint8_t *dbWriteCopy;
   Snap7plc *plc;

   Ui::Snap7fakeplcClass ui;
};
