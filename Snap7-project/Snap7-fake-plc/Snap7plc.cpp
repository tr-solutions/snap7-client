#include "Snap7plc.h"
#include "snap7.h"

Snap7plc::Snap7plc()
{
   pS7srv = new TS7Server();
}


Snap7plc::~Snap7plc()
{
}

void Snap7plc::RunServer(void) {

   pS7srv->Start();

}

void Snap7plc::SetDBReadArea(int DBNumber, int Size)
{
   dbread_no = DBNumber;
   dbread_size = Size;
   dbread_buffer = new uint8_t[Size];

   pS7srv->RegisterArea(srvAreaDB, DBNumber, (void*)dbread_buffer, Size);
   
}

void Snap7plc::SetDBWriteArea(int DBNumber,  int Size)
{
   dbwrite_no = DBNumber;
   dbwrite_size = Size;
   dbwrite_buffer = new uint8_t[Size];

   pS7srv->RegisterArea(srvAreaDB, DBNumber, (void*)dbwrite_buffer, Size);

}


void  Snap7plc::DBSetData(void *dbData, int dataSize)
{
   if (dbwrite_size >= dataSize) {
      if (nullptr != dbwrite_buffer)
         memcpy(dbwrite_buffer, dbData, dataSize);
   }
}

void Snap7plc::DBGetData(void *dbData, int dataSize)
{
   if (dbread_size <= dataSize) {
      if (nullptr != dbData && nullptr != dbread_buffer)
         memcpy(dbData, dbread_buffer, dataSize);
   }
}