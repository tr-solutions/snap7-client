#include "Snap7-client-plc.h"
#include "Snap7plc.h"

#include <QTimer>
#include <QMessageBox>
#include <qendian.h>

Snap7client::Snap7client(QWidget *parent)
    : QMainWindow(parent), dbReadBytes(32), dbWriteBytes(22), 
   dbErp2PlcMagicNoStart(0x12ffabcd), dbErp2PlcMagicNoEnd(~0x12ffabcd)
{
   plc = new Snap7plc();
   eventsRuning = false;
   ui.setupUi(this);
}

void Snap7client::insertLogLine(const QString& logText)
{
   ui.infoBarContent->insertPlainText(logText+QString("\n"));
   ui.infoBarContent->moveCursor(QTextCursor::End);
}

void Snap7client::insertLogDBReadRawData()
{
   QString    localTemporaryString;

   if (dbReadBytes > 0 && nullptr != dbReadCopy)
   {
      for (int i = 0; i < dbReadBytes; i++) {
     
         localTemporaryString += QString("%1").arg(dbReadCopy[i], 2, 16, QLatin1Char('0'));
         localTemporaryString += QString(" ");

      }
   }

   ui.plc2erpDataBlockContent->insertPlainText(localTemporaryString+QString("\n"));
   ui.plc2erpDataBlockContent->moveCursor(QTextCursor::End);
}

void Snap7client::insertLogDBWriteRawData()
{
   QString    localTemporaryString;

   if (dbWriteBytes > 0 && nullptr != dbWriteCopy)
   {
      for (int i = 0; i < dbWriteBytes; i++) {

         localTemporaryString += QString("%1").arg(dbWriteCopy[i], 2, 16, QLatin1Char('0'));
         localTemporaryString += QString(" ");

      }
   }


   ui.erp2plcDataBlockContent->insertPlainText(localTemporaryString + QString("\n"));
   ui.erp2plcDataBlockContent->moveCursor(QTextCursor::End);
}


void Snap7client::slotConnectToPLC()
{

   if (false == plc->isConnected())
   {

      insertLogLine("Connect request submited");

      // Temporarly ip address is hard coded
      plc->SetIpAddress("192.168.0.178");

     // plc->SetIpAddress("192.168.0.200");

      dbReadCopy = new uint8_t[dbReadBytes];
      dbWriteCopy = new uint8_t[dbWriteBytes];
      
      
      insertLogLine(QString("Initial info #1: DBRead size in bytes %1, dbRead structure size in bytes %2").arg(dbReadBytes, 0, 10).arg(sizeof(dbReadRawData_t), 0, 10));
      insertLogLine(QString("Initial info #2: DBWrite size in bytes %1, dbWrite structure size in bytes %2").arg(dbWriteBytes, 0, 10).arg(sizeof(dbWriteRawData_t), 0, 10));

      memset(dbWriteCopy, 0, dbWriteBytes);

      plc->SetDBReadArea(200, 0, dbReadBytes);
      plc->SetDBWriteArea(100, 0, dbWriteBytes);


      if (0 == plc->Connect()) {
         insertLogLine("PLC connected successfully");
      }
      else
      {
         insertLogLine("Can't connect to PLC - check power, connection, settings... and try again.");

         QMessageBox::warning(this, tr("Error"), tr(""
            "Can't connect to PLC - check power, connection, settings... and try again."),
            QMessageBox::Ok);
      }
   }
   else
   {
      insertLogLine("PLC already connected");
   }


}

void Snap7client::slotCheckConnect()
{

   if (plc->isConnected()) {
   // nothing to do
   }
}

void Snap7client::slotDisconnectPLC()
{

   if (plc->isConnected()) 
   {
      eventsRuning = false;
      insertLogLine("Disconnect request submited");
      QTimer::singleShot(50, this, SLOT(slotDisconnectEvent()));
   }
   else
   {
      insertLogLine("PLC already disconnected");
   }

}

void Snap7client::slotRunClient()
{
   if (false == eventsRuning) {
      insertLogLine("Run client request submited");

      QTimer::singleShot(50, this, SLOT(slotDBReadEvent()));
      QTimer::singleShot(1, this, SLOT(slotUpdateLiveCounterEvent()));
      eventsRuning = true;
   }

   else
   {
      insertLogLine("Data exchange already running, submit ignored");
   }

}

void Snap7client::slotAlarms()
{

}

void Snap7client::slotSettings()
{

}

void Snap7client::slotToggleEnableManualEditMagicStartERP(bool)
{

}

void Snap7client::slotToggleEnableManualEditMagicStopERP(bool)
{

}

void Snap7client::slotToggleEnableManualEditLiveCounterERP(bool)
{

}

void Snap7client::slotToggleEnableManualEditErrorIDERP(bool)
{


}

void Snap7client::slotDBReadEvent()
{

   if (true == eventsRuning) 
   {
      plc->DBRead();
      QTimer::singleShot(1, this, SLOT(slotDBReadCopyEvent()));
   }
   else
   {
      QTimer::singleShot(1, this, SLOT(slotDisconnectEvent()));
   }
}

void Snap7client::slotDBReadCopyEvent()
{
   pDbReadRawData_t temporaryDataPtr;
   uint16_t temporaryUint16;
   uint32_t temporaryUint32;

   

   if (true == eventsRuning)
   {
      plc->DBGetData(dbReadCopy, dbReadBytes);
      insertLogDBReadRawData();
      temporaryDataPtr = (pDbReadRawData_t)dbReadCopy;
      
      // Magic number start
      qToBigEndian<quint32>(temporaryDataPtr->MagicNumberStart, &temporaryUint32);
      ui.magicStartPLC->setText(QString("%1").arg(temporaryUint32, 8, 16, QLatin1Char('0')));

      // Live counter
      qToBigEndian<quint16>(temporaryDataPtr->LiveCounter, &temporaryUint16);
      ui.liveCounterPLC->setText(QString("%1").arg(temporaryUint16, 0, 10));

      // Machine ready
      switch (temporaryDataPtr->MachineReadyFlag) {
      case 0: ui.readyPLC->setStyleSheet(QString("background-color: rgb(140, 140, 140);")); break;
      case 1: ui.readyPLC->setStyleSheet(QString("background-color: rgb(0, 255, 0);")); break;
      }
      // Machine error
      switch (temporaryDataPtr->MachineErrorFlag) {
      case 0: ui.errorPLC->setStyleSheet(QString("background-color: rgb(140, 140, 140);")); break;
      case 1: ui.errorPLC->setStyleSheet(QString("background-color: rgb(0, 255, 0);")); break;
      }
      // Machine cycle run
      switch (temporaryDataPtr->MachineCycleRunningFlag) {
      case 0: ui.cycleRunPLC->setStyleSheet(QString("background-color: rgb(140, 140, 140);")); break;
      case 1: ui.cycleRunPLC->setStyleSheet(QString("background-color: rgb(0, 255, 0);")); break;
      }

      // Machine cycle pause
      switch (temporaryDataPtr->MachineCyclePausedFlag) {
      case 0: ui.cyclePausePLC->setStyleSheet(QString("background-color: rgb(140, 140, 140);")); break;
      case 1: ui.cyclePausePLC->setStyleSheet(QString("background-color: rgb(0, 255, 0);")); break;
      }
      // Machine cycle stop
      switch (temporaryDataPtr->MachineCycleStoppedFlag) {
      case 0: ui.cycleStopPLC->setStyleSheet(QString("background-color: rgb(140, 140, 140);")); break;
      case 1: ui.cycleStopPLC->setStyleSheet(QString("background-color: rgb(0, 255, 0);")); break;
      }

      // Machine step
      qToBigEndian<quint16>(temporaryDataPtr->MachineCycleStep, &temporaryUint16);
      ui.cycleStepPLC->setText(QString("%1").arg(temporaryUint16, 0, 10));

      // Product manufacturing
      switch (temporaryDataPtr->ProductManufacturing) {
      case 0: ui.productManufacturingPLC->setStyleSheet(QString("background-color: rgb(140, 140, 140);")); break;
      case 1: ui.productManufacturingPLC->setStyleSheet(QString("background-color: rgb(0, 255, 0);")); break;
      }

      // Product ready
      switch (temporaryDataPtr->ProductReady) {
      case 0: ui.productReadyPLC->setStyleSheet(QString("background-color: rgb(140, 140, 140);")); break;
      case 1: ui.productReadyPLC->setStyleSheet(QString("background-color: rgb(0, 255, 0);")); break;
      }

      // Product error
      switch (temporaryDataPtr->ProductError) {
      case 0: ui.productErrorPLC->setStyleSheet(QString("background-color: rgb(140, 140, 140);")); break;
      case 1: ui.productErrorPLC->setStyleSheet(QString("background-color: rgb(0, 255, 0);")); break;
      }

      // Product accepted
      switch (temporaryDataPtr->ProductAccepted) {
      case 0: ui.productAcceptedPLC->setStyleSheet(QString("background-color: rgb(140, 140, 140);")); break;
      case 1: ui.productAcceptedPLC->setStyleSheet(QString("background-color: rgb(0, 255, 0);")); break;
      }

      // Product invalid
      switch (temporaryDataPtr->ProductInvalid) {
      case 0: ui.productInvalidPLC->setStyleSheet(QString("background-color: rgb(140, 140, 140);")); break;
      case 1: ui.productInvalidPLC->setStyleSheet(QString("background-color: rgb(0, 255, 0);")); break;
      }

      // Product loaded
      switch (temporaryDataPtr->ProductLoaded) {
      case 0: ui.productLoadedPLC->setStyleSheet(QString("background-color: rgb(140, 140, 140);")); break;
      case 1: ui.productLoadedPLC->setStyleSheet(QString("background-color: rgb(0, 255, 0);")); break;
      }

      // Product type
      qToBigEndian<quint16>(temporaryDataPtr->ProductType, &temporaryUint16);
      ui.productTypePLC->setText(QString("%1").arg(temporaryUint16, 0, 10));

      // Product qty
      qToBigEndian<quint16>(temporaryDataPtr->ProductQty, &temporaryUint16);
      ui.productQtyPLC->setText(QString("%1").arg(temporaryUint16, 0, 10));


      // Magic number end
      qToBigEndian<quint32>(temporaryDataPtr->MagicNumberEnd, &temporaryUint32);
      ui.magicEndPLC->setText(QString("%1").arg(temporaryUint32, 8, 16, QLatin1Char('0')));

      if (temporaryDataPtr->AlarmsBits > 0 ||
         temporaryDataPtr->Reel1ErrorBits > 0 || temporaryDataPtr->Reel2ErrorBits > 0 ||
         temporaryDataPtr->Reel3ErrorBits > 0 || temporaryDataPtr->Reel4ErrorBits > 0)
      {
         ui.showAlarmsPb->setStyleSheet(QString("background-color: rgb(255, 0, 0);"));
      }
      else 
      {
         ui.showAlarmsPb->setStyleSheet(QString("background-color: rgb(200, 200, 200);"));
      }

      QTimer::singleShot(1, this, SLOT(slotDBWriteCopyEvent()));
   }
   else
   {
      QTimer::singleShot(1, this, SLOT(slotDisconnectEvent()));
   }
}

void Snap7client::slotDBWriteEvent()
{
   if (true == eventsRuning)
   {
      plc->DBWrite();
      QTimer::singleShot(50, this, SLOT(slotDBReadEvent()));
   }
   else
   {
      QTimer::singleShot(1, this, SLOT(slotDisconnectEvent()));
   }
}

void Snap7client::slotDBWriteCopyEvent()
{
   if (true == eventsRuning)
   {
      plc->DBSetData(dbWriteCopy, dbWriteBytes);
      QTimer::singleShot(1, this, SLOT(slotDBWriteEvent()));
   }
   else
   {
      QTimer::singleShot(1, this, SLOT(slotDisconnectEvent()));
   }

}

void Snap7client::slotDisconnectEvent()
{
   plc->Disconnect();
}

void Snap7client::slotUpdateLiveCounterEvent()
{
   uint16_t temporaryUint16;
   pDbWriteRawData_t temporaryDataPtr = (pDbWriteRawData_t)dbWriteCopy;

   qToBigEndian<quint16>(temporaryDataPtr->LiveCounter, &temporaryUint16);
   ++temporaryUint16;
   qToBigEndian<quint16>(temporaryUint16, &temporaryDataPtr->LiveCounter);

   temporaryDataPtr->RequestNewProductFlag = 1;
   temporaryDataPtr->ProductStoredFlag = 1;
   temporaryDataPtr->ProductType = 0x1;
   temporaryDataPtr->ProductQty = 0x1;
   temporaryDataPtr->MagicNumberStart = 0xAABB5566;
   temporaryDataPtr->MagicNumberEnd = 0x11223344;
   QTimer::singleShot(1000, this, SLOT(slotUpdateLiveCounterEvent()));
   QTimer::singleShot(1, this, SLOT(slotDBWriteCopyEvent()));

}

