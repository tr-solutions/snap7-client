#pragma once

class TS7Client;

class Snap7plc
{
public:
   Snap7plc();
   ~Snap7plc();

   void SetIpAddress(const char* ipAddress);
   void SetDBReadArea(int DBNumber, int Start, int Size);
   void SetDBWriteArea(int DBNumber, int Start, int Size);

   int  Connect();
   bool isConnected();

   int  Disconnect();
   int  DBRead();
   int  DBWrite();

   void DBGetData(void *dbData, int dataSize);
   void DBSetData(void *,int dataSize);
private:

   int ip_addr_len;
   char *ip_address;

   int dbread_no;
   int dbread_start;
   int dbread_size;
   void *dbread_buffer;

   int dbwrite_no;
   int dbwrite_start;
   int dbwrite_size;
   void *dbwrite_buffer;

   TS7Client *client;

};

