#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_Snap7project.h"

class Snap7plc;
class QTimer;
class QString;

class Snap7client : public QMainWindow
{
    Q_OBJECT

    typedef struct {
       uint32_t MagicNumberStart;
       uint16_t LiveCounter;

       union {
          uint16_t StatusBits;
          struct {
             uint16_t MachineReadyFlag : 1;
             uint16_t MachineErrorFlag : 1;
             uint16_t MachineCycleRunningFlag : 1;
             uint16_t MachineCycleStoppedFlag : 1;
             uint16_t MachineCyclePausedFlag : 1;
             uint16_t : 11;
          };
       };

       int16_t MachineCycleStep;
       
       union {
          uint16_t AlarmsBits;
          struct {
             uint16_t DoorsOpenedFlag : 1;
             uint16_t EstopActiveFlag : 1;
             uint16_t OvertravelLeftFlag : 1;
             uint16_t OvertravelRightFlag : 1;
             uint16_t GrabberErrorFlag : 1;
             uint16_t TraverseErrorFlag : 1;
             uint16_t InverterErrorFlag : 1;
             uint16_t : 9;
          };
       };

       union {
          uint16_t Reel1ErrorBits;
          struct {
             uint16_t Reel1Empty : 1;
             uint16_t Reel1Fault : 1;
             uint16_t : 14;
          };
       };

       union {
          uint16_t Reel2ErrorBits;
          struct {
             uint16_t Reel2Empty : 1;
             uint16_t Reel2Fault : 1;
             uint16_t : 14;
          };
       };

       union {
          uint16_t Reel3ErrorBits;
          struct {
             uint16_t Reel3Empty : 1;
             uint16_t Reel3Fault : 1;
             uint16_t : 14;
          };
       };

       union {
          uint16_t Reel4ErrorBits;
          struct {
             uint16_t Reel4Empty : 1;
             uint16_t Reel4Fault : 1;
             uint16_t : 14;
          };
       };

       union {
          uint16_t ReservedControlBits;
          struct {
             uint16_t ReservedControlBit_0 : 1;
             uint16_t : 15;
          };
       };

       union {
          uint16_t ProductInfoBits;
          struct {
             uint16_t ProductManufacturing : 1;
             uint16_t ProductReady : 1;
             uint16_t ProductError : 1;
             uint16_t ProductAccepted : 1;
             uint16_t ProductInvalid : 1;
             uint16_t ProductLoaded : 1;
             uint16_t : 10;
          };
       };

       uint16_t ProductType;
       uint16_t ProductQty;
       uint32_t MagicNumberEnd;

    } dbReadRawData_t, *pDbReadRawData_t;

#pragma pack(push, 1)
    typedef struct {


       uint32_t MagicNumberStart;
       uint16_t LiveCounter;
       union {
          uint16_t StatusBits;
          struct {
             uint16_t ErpOkFlag : 1;
             uint16_t EropErrorFlag : 1;
             uint16_t : 14;
          };
       };
       uint16_t ErpErrorID;
       union {
          uint16_t ControlBits;
          struct {
             uint16_t RequestNewProductFlag : 1;
             uint16_t RequestStartCycleFlag : 1;
             uint16_t RequestStopCycleFlag : 1;
             uint16_t RequestPauseCycleFlag : 1;
             uint16_t RequestErrorResetFlag : 1;
             uint16_t : 11;
          };
       };
       union {
          uint16_t StatusBits2;
          struct {
             uint16_t ProductStoredFlag : 1;
             uint16_t : 15;
          };
       };

       uint16_t ProductType;
       uint16_t ProductQty;
       uint32_t MagicNumberEnd;


    } dbWriteRawData_t, *pDbWriteRawData_t;
#pragma pack(pop)

public:
    Snap7client(QWidget *parent = Q_NULLPTR);

private slots:
   void slotConnectToPLC();
   void slotCheckConnect();
   void slotDisconnectPLC();
   void slotRunClient();
   void slotAlarms();
   void slotSettings();
   void slotToggleEnableManualEditMagicStartERP(bool);
   void slotToggleEnableManualEditMagicStopERP(bool);
   void slotToggleEnableManualEditLiveCounterERP(bool);
   void slotToggleEnableManualEditErrorIDERP(bool);
   void slotDBReadEvent();
   void slotDBReadCopyEvent();
   void slotDBWriteEvent();
   void slotDBWriteCopyEvent();
   void slotDisconnectEvent();
   void slotUpdateLiveCounterEvent();

private :

   void insertLogLine(const QString&);
   void insertLogDBReadRawData();
   void insertLogDBWriteRawData();

private:

   const uint16_t dbReadBytes;
   const uint16_t dbWriteBytes;
   const uint32_t dbErp2PlcMagicNoStart;
   const uint32_t dbErp2PlcMagicNoEnd;

   bool eventsRuning;

   uint8_t *dbReadCopy;
   uint8_t *dbWriteCopy;

   Snap7plc *plc;

   Ui::Snap7projectClass ui;
};
