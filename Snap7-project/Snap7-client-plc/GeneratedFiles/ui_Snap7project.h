/********************************************************************************
** Form generated from reading UI file 'Snap7project.ui'
**
** Created by: Qt User Interface Compiler version 5.9.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SNAP7PROJECT_H
#define UI_SNAP7PROJECT_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Snap7projectClass
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout_5;
    QPushButton *connectToPlcPb;
    QPushButton *runClientPb;
    QHBoxLayout *horizontalLayout_29;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLineEdit *magicStartERP;
    QCheckBox *enableManualEditMagicStartERP;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_3;
    QLineEdit *liveCounterERP;
    QCheckBox *enableManualEditLiveCounterERP;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_4;
    QPushButton *toggleOkStatusERP;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_5;
    QPushButton *toggleErrorSatusERP;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_6;
    QSpinBox *errorIDERP;
    QCheckBox *enableManualEditErrorIDERP;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_7;
    QPushButton *submitNewProductERP;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_8;
    QPushButton *submitRequestStartCycleERP;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_9;
    QPushButton *submitRequestPauseCycleERP;
    QHBoxLayout *horizontalLayout_10;
    QLabel *label_10;
    QPushButton *submitRequestStopCycleERP;
    QHBoxLayout *horizontalLayout_11;
    QLabel *label_11;
    QPushButton *submitRequestErrorResetERP;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_12;
    QLabel *label_12;
    QPushButton *submitProductStoredERP;
    QFrame *line;
    QHBoxLayout *horizontalLayout_13;
    QLabel *label_13;
    QComboBox *selectProductTypeERP;
    QHBoxLayout *horizontalLayout_14;
    QLabel *label_14;
    QSpinBox *productQtyERP;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QLineEdit *magicEndERP;
    QCheckBox *enableManualEditMagicEndERP;
    QFrame *line_2;
    QPlainTextEdit *erp2plcDataBlockContent;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_15;
    QLabel *label_15;
    QLineEdit *magicStartPLC;
    QHBoxLayout *horizontalLayout_16;
    QLabel *label_16;
    QLineEdit *liveCounterPLC;
    QHBoxLayout *horizontalLayout_17;
    QLabel *label_17;
    QLabel *readyPLC;
    QHBoxLayout *horizontalLayout_18;
    QLabel *label_18;
    QLabel *errorPLC;
    QHBoxLayout *horizontalLayout_31;
    QLabel *label_30;
    QLabel *cycleRunPLC;
    QHBoxLayout *horizontalLayout_32;
    QLabel *label_31;
    QLabel *cyclePausePLC;
    QHBoxLayout *horizontalLayout_33;
    QLabel *label_32;
    QLabel *cycleStopPLC;
    QHBoxLayout *horizontalLayout_34;
    QLabel *label_33;
    QLineEdit *cycleStepPLC;
    QHBoxLayout *horizontalLayout_20;
    QLabel *label_20;
    QLabel *productManufacturingPLC;
    QHBoxLayout *horizontalLayout_21;
    QLabel *label_21;
    QLabel *productReadyPLC;
    QHBoxLayout *horizontalLayout_22;
    QLabel *label_22;
    QLabel *productErrorPLC;
    QHBoxLayout *horizontalLayout_23;
    QLabel *label_23;
    QLabel *productAcceptedPLC;
    QHBoxLayout *horizontalLayout_24;
    QLabel *label_24;
    QLabel *productInvalidPLC;
    QHBoxLayout *horizontalLayout_25;
    QLabel *label_25;
    QLabel *productLoadedPLC;
    QGroupBox *groupBox_4;
    QVBoxLayout *verticalLayout_4;
    QFrame *line_3;
    QHBoxLayout *horizontalLayout_26;
    QLabel *label_26;
    QLineEdit *productTypePLC;
    QHBoxLayout *horizontalLayout_27;
    QLabel *label_27;
    QLineEdit *productQtyPLC;
    QHBoxLayout *horizontalLayout_28;
    QLabel *label_28;
    QLineEdit *magicEndPLC;
    QFrame *line_4;
    QPlainTextEdit *plc2erpDataBlockContent;
    QPushButton *showAlarmsPb;
    QPlainTextEdit *infoBarContent;
    QPushButton *showSettingsPb;
    QPushButton *disconnectPb;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *Snap7projectClass)
    {
        if (Snap7projectClass->objectName().isEmpty())
            Snap7projectClass->setObjectName(QStringLiteral("Snap7projectClass"));
        Snap7projectClass->resize(1804, 1747);
        centralWidget = new QWidget(Snap7projectClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout_5 = new QVBoxLayout(centralWidget);
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        connectToPlcPb = new QPushButton(centralWidget);
        connectToPlcPb->setObjectName(QStringLiteral("connectToPlcPb"));

        verticalLayout_5->addWidget(connectToPlcPb);

        runClientPb = new QPushButton(centralWidget);
        runClientPb->setObjectName(QStringLiteral("runClientPb"));

        verticalLayout_5->addWidget(runClientPb);

        horizontalLayout_29 = new QHBoxLayout();
        horizontalLayout_29->setSpacing(6);
        horizontalLayout_29->setObjectName(QStringLiteral("horizontalLayout_29"));
        horizontalLayout_29->setContentsMargins(-1, 20, -1, -1);
        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout->addWidget(label);

        magicStartERP = new QLineEdit(groupBox);
        magicStartERP->setObjectName(QStringLiteral("magicStartERP"));
        magicStartERP->setInputMethodHints(Qt::ImhDigitsOnly);
        magicStartERP->setReadOnly(true);

        horizontalLayout->addWidget(magicStartERP);

        enableManualEditMagicStartERP = new QCheckBox(groupBox);
        enableManualEditMagicStartERP->setObjectName(QStringLiteral("enableManualEditMagicStartERP"));
        enableManualEditMagicStartERP->setMaximumSize(QSize(16, 16777215));

        horizontalLayout->addWidget(enableManualEditMagicStartERP);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QStringLiteral("label_3"));

        horizontalLayout_3->addWidget(label_3);

        liveCounterERP = new QLineEdit(groupBox);
        liveCounterERP->setObjectName(QStringLiteral("liveCounterERP"));
        liveCounterERP->setInputMethodHints(Qt::ImhDigitsOnly);
        liveCounterERP->setReadOnly(true);

        horizontalLayout_3->addWidget(liveCounterERP);

        enableManualEditLiveCounterERP = new QCheckBox(groupBox);
        enableManualEditLiveCounterERP->setObjectName(QStringLiteral("enableManualEditLiveCounterERP"));
        enableManualEditLiveCounterERP->setMaximumSize(QSize(16, 16777215));

        horizontalLayout_3->addWidget(enableManualEditLiveCounterERP);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QStringLiteral("label_4"));

        horizontalLayout_4->addWidget(label_4);

        toggleOkStatusERP = new QPushButton(groupBox);
        toggleOkStatusERP->setObjectName(QStringLiteral("toggleOkStatusERP"));

        horizontalLayout_4->addWidget(toggleOkStatusERP);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QStringLiteral("label_5"));

        horizontalLayout_5->addWidget(label_5);

        toggleErrorSatusERP = new QPushButton(groupBox);
        toggleErrorSatusERP->setObjectName(QStringLiteral("toggleErrorSatusERP"));

        horizontalLayout_5->addWidget(toggleErrorSatusERP);


        verticalLayout->addLayout(horizontalLayout_5);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QStringLiteral("label_6"));

        horizontalLayout_6->addWidget(label_6);

        errorIDERP = new QSpinBox(groupBox);
        errorIDERP->setObjectName(QStringLiteral("errorIDERP"));
        errorIDERP->setReadOnly(true);

        horizontalLayout_6->addWidget(errorIDERP);

        enableManualEditErrorIDERP = new QCheckBox(groupBox);
        enableManualEditErrorIDERP->setObjectName(QStringLiteral("enableManualEditErrorIDERP"));
        enableManualEditErrorIDERP->setMaximumSize(QSize(16, 16777215));

        horizontalLayout_6->addWidget(enableManualEditErrorIDERP);


        verticalLayout->addLayout(horizontalLayout_6);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QStringLiteral("label_7"));

        horizontalLayout_7->addWidget(label_7);

        submitNewProductERP = new QPushButton(groupBox);
        submitNewProductERP->setObjectName(QStringLiteral("submitNewProductERP"));

        horizontalLayout_7->addWidget(submitNewProductERP);


        verticalLayout->addLayout(horizontalLayout_7);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setObjectName(QStringLiteral("horizontalLayout_8"));
        label_8 = new QLabel(groupBox);
        label_8->setObjectName(QStringLiteral("label_8"));

        horizontalLayout_8->addWidget(label_8);

        submitRequestStartCycleERP = new QPushButton(groupBox);
        submitRequestStartCycleERP->setObjectName(QStringLiteral("submitRequestStartCycleERP"));

        horizontalLayout_8->addWidget(submitRequestStartCycleERP);


        verticalLayout->addLayout(horizontalLayout_8);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(6);
        horizontalLayout_9->setObjectName(QStringLiteral("horizontalLayout_9"));
        label_9 = new QLabel(groupBox);
        label_9->setObjectName(QStringLiteral("label_9"));

        horizontalLayout_9->addWidget(label_9);

        submitRequestPauseCycleERP = new QPushButton(groupBox);
        submitRequestPauseCycleERP->setObjectName(QStringLiteral("submitRequestPauseCycleERP"));

        horizontalLayout_9->addWidget(submitRequestPauseCycleERP);


        verticalLayout->addLayout(horizontalLayout_9);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setSpacing(6);
        horizontalLayout_10->setObjectName(QStringLiteral("horizontalLayout_10"));
        label_10 = new QLabel(groupBox);
        label_10->setObjectName(QStringLiteral("label_10"));

        horizontalLayout_10->addWidget(label_10);

        submitRequestStopCycleERP = new QPushButton(groupBox);
        submitRequestStopCycleERP->setObjectName(QStringLiteral("submitRequestStopCycleERP"));

        horizontalLayout_10->addWidget(submitRequestStopCycleERP);


        verticalLayout->addLayout(horizontalLayout_10);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setSpacing(6);
        horizontalLayout_11->setObjectName(QStringLiteral("horizontalLayout_11"));
        label_11 = new QLabel(groupBox);
        label_11->setObjectName(QStringLiteral("label_11"));

        horizontalLayout_11->addWidget(label_11);

        submitRequestErrorResetERP = new QPushButton(groupBox);
        submitRequestErrorResetERP->setObjectName(QStringLiteral("submitRequestErrorResetERP"));

        horizontalLayout_11->addWidget(submitRequestErrorResetERP);


        verticalLayout->addLayout(horizontalLayout_11);

        groupBox_3 = new QGroupBox(groupBox);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        verticalLayout_2 = new QVBoxLayout(groupBox_3);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setSpacing(6);
        horizontalLayout_12->setObjectName(QStringLiteral("horizontalLayout_12"));
        label_12 = new QLabel(groupBox_3);
        label_12->setObjectName(QStringLiteral("label_12"));

        horizontalLayout_12->addWidget(label_12);

        submitProductStoredERP = new QPushButton(groupBox_3);
        submitProductStoredERP->setObjectName(QStringLiteral("submitProductStoredERP"));

        horizontalLayout_12->addWidget(submitProductStoredERP);


        verticalLayout_2->addLayout(horizontalLayout_12);

        line = new QFrame(groupBox_3);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout_2->addWidget(line);

        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setSpacing(6);
        horizontalLayout_13->setObjectName(QStringLiteral("horizontalLayout_13"));
        label_13 = new QLabel(groupBox_3);
        label_13->setObjectName(QStringLiteral("label_13"));

        horizontalLayout_13->addWidget(label_13);

        selectProductTypeERP = new QComboBox(groupBox_3);
        selectProductTypeERP->setObjectName(QStringLiteral("selectProductTypeERP"));

        horizontalLayout_13->addWidget(selectProductTypeERP);


        verticalLayout_2->addLayout(horizontalLayout_13);

        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setSpacing(6);
        horizontalLayout_14->setObjectName(QStringLiteral("horizontalLayout_14"));
        label_14 = new QLabel(groupBox_3);
        label_14->setObjectName(QStringLiteral("label_14"));

        horizontalLayout_14->addWidget(label_14);

        productQtyERP = new QSpinBox(groupBox_3);
        productQtyERP->setObjectName(QStringLiteral("productQtyERP"));
        productQtyERP->setReadOnly(false);
        productQtyERP->setDisplayIntegerBase(16);

        horizontalLayout_14->addWidget(productQtyERP);


        verticalLayout_2->addLayout(horizontalLayout_14);


        verticalLayout->addWidget(groupBox_3);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout_2->addWidget(label_2);

        magicEndERP = new QLineEdit(groupBox);
        magicEndERP->setObjectName(QStringLiteral("magicEndERP"));
        magicEndERP->setInputMethodHints(Qt::ImhDigitsOnly);
        magicEndERP->setInputMask(QStringLiteral(""));
        magicEndERP->setAlignment(Qt::AlignCenter);
        magicEndERP->setReadOnly(true);

        horizontalLayout_2->addWidget(magicEndERP);

        enableManualEditMagicEndERP = new QCheckBox(groupBox);
        enableManualEditMagicEndERP->setObjectName(QStringLiteral("enableManualEditMagicEndERP"));
        enableManualEditMagicEndERP->setMaximumSize(QSize(16, 16777215));

        horizontalLayout_2->addWidget(enableManualEditMagicEndERP);


        verticalLayout->addLayout(horizontalLayout_2);

        line_2 = new QFrame(groupBox);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line_2);

        erp2plcDataBlockContent = new QPlainTextEdit(groupBox);
        erp2plcDataBlockContent->setObjectName(QStringLiteral("erp2plcDataBlockContent"));
        erp2plcDataBlockContent->setReadOnly(true);

        verticalLayout->addWidget(erp2plcDataBlockContent);


        horizontalLayout_29->addWidget(groupBox);

        groupBox_2 = new QGroupBox(centralWidget);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        verticalLayout_3 = new QVBoxLayout(groupBox_2);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        horizontalLayout_15 = new QHBoxLayout();
        horizontalLayout_15->setSpacing(6);
        horizontalLayout_15->setObjectName(QStringLiteral("horizontalLayout_15"));
        label_15 = new QLabel(groupBox_2);
        label_15->setObjectName(QStringLiteral("label_15"));

        horizontalLayout_15->addWidget(label_15);

        magicStartPLC = new QLineEdit(groupBox_2);
        magicStartPLC->setObjectName(QStringLiteral("magicStartPLC"));
        magicStartPLC->setInputMethodHints(Qt::ImhDigitsOnly);
        magicStartPLC->setReadOnly(true);

        horizontalLayout_15->addWidget(magicStartPLC);


        verticalLayout_3->addLayout(horizontalLayout_15);

        horizontalLayout_16 = new QHBoxLayout();
        horizontalLayout_16->setSpacing(6);
        horizontalLayout_16->setObjectName(QStringLiteral("horizontalLayout_16"));
        label_16 = new QLabel(groupBox_2);
        label_16->setObjectName(QStringLiteral("label_16"));

        horizontalLayout_16->addWidget(label_16);

        liveCounterPLC = new QLineEdit(groupBox_2);
        liveCounterPLC->setObjectName(QStringLiteral("liveCounterPLC"));
        liveCounterPLC->setInputMethodHints(Qt::ImhDigitsOnly);
        liveCounterPLC->setReadOnly(true);

        horizontalLayout_16->addWidget(liveCounterPLC);


        verticalLayout_3->addLayout(horizontalLayout_16);

        horizontalLayout_17 = new QHBoxLayout();
        horizontalLayout_17->setSpacing(6);
        horizontalLayout_17->setObjectName(QStringLiteral("horizontalLayout_17"));
        label_17 = new QLabel(groupBox_2);
        label_17->setObjectName(QStringLiteral("label_17"));

        horizontalLayout_17->addWidget(label_17);

        readyPLC = new QLabel(groupBox_2);
        readyPLC->setObjectName(QStringLiteral("readyPLC"));
        readyPLC->setAutoFillBackground(false);
        readyPLC->setStyleSheet(QStringLiteral("background-color: rgb(0, 255, 0);"));
        readyPLC->setFrameShape(QFrame::Box);
        readyPLC->setTextFormat(Qt::PlainText);

        horizontalLayout_17->addWidget(readyPLC);


        verticalLayout_3->addLayout(horizontalLayout_17);

        horizontalLayout_18 = new QHBoxLayout();
        horizontalLayout_18->setSpacing(6);
        horizontalLayout_18->setObjectName(QStringLiteral("horizontalLayout_18"));
        label_18 = new QLabel(groupBox_2);
        label_18->setObjectName(QStringLiteral("label_18"));

        horizontalLayout_18->addWidget(label_18);

        errorPLC = new QLabel(groupBox_2);
        errorPLC->setObjectName(QStringLiteral("errorPLC"));
        errorPLC->setAutoFillBackground(false);
        errorPLC->setStyleSheet(QStringLiteral("background-color: rgb(0, 255, 0);"));
        errorPLC->setFrameShape(QFrame::Box);
        errorPLC->setTextFormat(Qt::PlainText);

        horizontalLayout_18->addWidget(errorPLC);


        verticalLayout_3->addLayout(horizontalLayout_18);

        horizontalLayout_31 = new QHBoxLayout();
        horizontalLayout_31->setSpacing(6);
        horizontalLayout_31->setObjectName(QStringLiteral("horizontalLayout_31"));
        label_30 = new QLabel(groupBox_2);
        label_30->setObjectName(QStringLiteral("label_30"));

        horizontalLayout_31->addWidget(label_30);

        cycleRunPLC = new QLabel(groupBox_2);
        cycleRunPLC->setObjectName(QStringLiteral("cycleRunPLC"));
        cycleRunPLC->setAutoFillBackground(false);
        cycleRunPLC->setStyleSheet(QStringLiteral("background-color: rgb(0, 255, 0);"));
        cycleRunPLC->setFrameShape(QFrame::Box);
        cycleRunPLC->setTextFormat(Qt::PlainText);

        horizontalLayout_31->addWidget(cycleRunPLC);


        verticalLayout_3->addLayout(horizontalLayout_31);

        horizontalLayout_32 = new QHBoxLayout();
        horizontalLayout_32->setSpacing(6);
        horizontalLayout_32->setObjectName(QStringLiteral("horizontalLayout_32"));
        label_31 = new QLabel(groupBox_2);
        label_31->setObjectName(QStringLiteral("label_31"));

        horizontalLayout_32->addWidget(label_31);

        cyclePausePLC = new QLabel(groupBox_2);
        cyclePausePLC->setObjectName(QStringLiteral("cyclePausePLC"));
        cyclePausePLC->setAutoFillBackground(false);
        cyclePausePLC->setStyleSheet(QStringLiteral("background-color: rgb(0, 255, 0);"));
        cyclePausePLC->setFrameShape(QFrame::Box);
        cyclePausePLC->setTextFormat(Qt::PlainText);

        horizontalLayout_32->addWidget(cyclePausePLC);


        verticalLayout_3->addLayout(horizontalLayout_32);

        horizontalLayout_33 = new QHBoxLayout();
        horizontalLayout_33->setSpacing(6);
        horizontalLayout_33->setObjectName(QStringLiteral("horizontalLayout_33"));
        label_32 = new QLabel(groupBox_2);
        label_32->setObjectName(QStringLiteral("label_32"));

        horizontalLayout_33->addWidget(label_32);

        cycleStopPLC = new QLabel(groupBox_2);
        cycleStopPLC->setObjectName(QStringLiteral("cycleStopPLC"));
        cycleStopPLC->setAutoFillBackground(false);
        cycleStopPLC->setStyleSheet(QStringLiteral("background-color: rgb(0, 255, 0);"));
        cycleStopPLC->setFrameShape(QFrame::Box);
        cycleStopPLC->setTextFormat(Qt::PlainText);

        horizontalLayout_33->addWidget(cycleStopPLC);


        verticalLayout_3->addLayout(horizontalLayout_33);

        horizontalLayout_34 = new QHBoxLayout();
        horizontalLayout_34->setSpacing(6);
        horizontalLayout_34->setObjectName(QStringLiteral("horizontalLayout_34"));
        label_33 = new QLabel(groupBox_2);
        label_33->setObjectName(QStringLiteral("label_33"));

        horizontalLayout_34->addWidget(label_33);

        cycleStepPLC = new QLineEdit(groupBox_2);
        cycleStepPLC->setObjectName(QStringLiteral("cycleStepPLC"));
        cycleStepPLC->setInputMethodHints(Qt::ImhDigitsOnly);
        cycleStepPLC->setReadOnly(true);

        horizontalLayout_34->addWidget(cycleStepPLC);


        verticalLayout_3->addLayout(horizontalLayout_34);

        horizontalLayout_20 = new QHBoxLayout();
        horizontalLayout_20->setSpacing(6);
        horizontalLayout_20->setObjectName(QStringLiteral("horizontalLayout_20"));
        label_20 = new QLabel(groupBox_2);
        label_20->setObjectName(QStringLiteral("label_20"));

        horizontalLayout_20->addWidget(label_20);

        productManufacturingPLC = new QLabel(groupBox_2);
        productManufacturingPLC->setObjectName(QStringLiteral("productManufacturingPLC"));
        productManufacturingPLC->setAutoFillBackground(false);
        productManufacturingPLC->setStyleSheet(QStringLiteral("background-color: rgb(0, 255, 0);"));
        productManufacturingPLC->setFrameShape(QFrame::Box);
        productManufacturingPLC->setTextFormat(Qt::PlainText);

        horizontalLayout_20->addWidget(productManufacturingPLC);


        verticalLayout_3->addLayout(horizontalLayout_20);

        horizontalLayout_21 = new QHBoxLayout();
        horizontalLayout_21->setSpacing(6);
        horizontalLayout_21->setObjectName(QStringLiteral("horizontalLayout_21"));
        label_21 = new QLabel(groupBox_2);
        label_21->setObjectName(QStringLiteral("label_21"));

        horizontalLayout_21->addWidget(label_21);

        productReadyPLC = new QLabel(groupBox_2);
        productReadyPLC->setObjectName(QStringLiteral("productReadyPLC"));
        productReadyPLC->setAutoFillBackground(false);
        productReadyPLC->setStyleSheet(QStringLiteral("background-color: rgb(0, 255, 0);"));
        productReadyPLC->setFrameShape(QFrame::Box);
        productReadyPLC->setTextFormat(Qt::PlainText);

        horizontalLayout_21->addWidget(productReadyPLC);


        verticalLayout_3->addLayout(horizontalLayout_21);

        horizontalLayout_22 = new QHBoxLayout();
        horizontalLayout_22->setSpacing(6);
        horizontalLayout_22->setObjectName(QStringLiteral("horizontalLayout_22"));
        label_22 = new QLabel(groupBox_2);
        label_22->setObjectName(QStringLiteral("label_22"));

        horizontalLayout_22->addWidget(label_22);

        productErrorPLC = new QLabel(groupBox_2);
        productErrorPLC->setObjectName(QStringLiteral("productErrorPLC"));
        productErrorPLC->setAutoFillBackground(false);
        productErrorPLC->setStyleSheet(QStringLiteral("background-color: rgb(0, 255, 0);"));
        productErrorPLC->setFrameShape(QFrame::Box);
        productErrorPLC->setTextFormat(Qt::PlainText);

        horizontalLayout_22->addWidget(productErrorPLC);


        verticalLayout_3->addLayout(horizontalLayout_22);

        horizontalLayout_23 = new QHBoxLayout();
        horizontalLayout_23->setSpacing(6);
        horizontalLayout_23->setObjectName(QStringLiteral("horizontalLayout_23"));
        label_23 = new QLabel(groupBox_2);
        label_23->setObjectName(QStringLiteral("label_23"));

        horizontalLayout_23->addWidget(label_23);

        productAcceptedPLC = new QLabel(groupBox_2);
        productAcceptedPLC->setObjectName(QStringLiteral("productAcceptedPLC"));
        productAcceptedPLC->setAutoFillBackground(false);
        productAcceptedPLC->setStyleSheet(QStringLiteral("background-color: rgb(0, 255, 0);"));
        productAcceptedPLC->setFrameShape(QFrame::Box);
        productAcceptedPLC->setTextFormat(Qt::PlainText);

        horizontalLayout_23->addWidget(productAcceptedPLC);


        verticalLayout_3->addLayout(horizontalLayout_23);

        horizontalLayout_24 = new QHBoxLayout();
        horizontalLayout_24->setSpacing(6);
        horizontalLayout_24->setObjectName(QStringLiteral("horizontalLayout_24"));
        label_24 = new QLabel(groupBox_2);
        label_24->setObjectName(QStringLiteral("label_24"));

        horizontalLayout_24->addWidget(label_24);

        productInvalidPLC = new QLabel(groupBox_2);
        productInvalidPLC->setObjectName(QStringLiteral("productInvalidPLC"));
        productInvalidPLC->setAutoFillBackground(false);
        productInvalidPLC->setStyleSheet(QStringLiteral("background-color: rgb(0, 255, 0);"));
        productInvalidPLC->setFrameShape(QFrame::Box);
        productInvalidPLC->setTextFormat(Qt::PlainText);

        horizontalLayout_24->addWidget(productInvalidPLC);


        verticalLayout_3->addLayout(horizontalLayout_24);

        horizontalLayout_25 = new QHBoxLayout();
        horizontalLayout_25->setSpacing(6);
        horizontalLayout_25->setObjectName(QStringLiteral("horizontalLayout_25"));
        label_25 = new QLabel(groupBox_2);
        label_25->setObjectName(QStringLiteral("label_25"));

        horizontalLayout_25->addWidget(label_25);

        productLoadedPLC = new QLabel(groupBox_2);
        productLoadedPLC->setObjectName(QStringLiteral("productLoadedPLC"));
        productLoadedPLC->setAutoFillBackground(false);
        productLoadedPLC->setStyleSheet(QStringLiteral("background-color: rgb(0, 255, 0);"));
        productLoadedPLC->setFrameShape(QFrame::Box);
        productLoadedPLC->setTextFormat(Qt::PlainText);

        horizontalLayout_25->addWidget(productLoadedPLC);


        verticalLayout_3->addLayout(horizontalLayout_25);

        groupBox_4 = new QGroupBox(groupBox_2);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
        verticalLayout_4 = new QVBoxLayout(groupBox_4);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        line_3 = new QFrame(groupBox_4);
        line_3->setObjectName(QStringLiteral("line_3"));
        line_3->setFrameShape(QFrame::HLine);
        line_3->setFrameShadow(QFrame::Sunken);

        verticalLayout_4->addWidget(line_3);

        horizontalLayout_26 = new QHBoxLayout();
        horizontalLayout_26->setSpacing(6);
        horizontalLayout_26->setObjectName(QStringLiteral("horizontalLayout_26"));
        label_26 = new QLabel(groupBox_4);
        label_26->setObjectName(QStringLiteral("label_26"));

        horizontalLayout_26->addWidget(label_26);

        productTypePLC = new QLineEdit(groupBox_4);
        productTypePLC->setObjectName(QStringLiteral("productTypePLC"));
        productTypePLC->setReadOnly(true);

        horizontalLayout_26->addWidget(productTypePLC);


        verticalLayout_4->addLayout(horizontalLayout_26);

        horizontalLayout_27 = new QHBoxLayout();
        horizontalLayout_27->setSpacing(6);
        horizontalLayout_27->setObjectName(QStringLiteral("horizontalLayout_27"));
        label_27 = new QLabel(groupBox_4);
        label_27->setObjectName(QStringLiteral("label_27"));

        horizontalLayout_27->addWidget(label_27);

        productQtyPLC = new QLineEdit(groupBox_4);
        productQtyPLC->setObjectName(QStringLiteral("productQtyPLC"));
        productQtyPLC->setInputMethodHints(Qt::ImhDigitsOnly);
        productQtyPLC->setReadOnly(true);

        horizontalLayout_27->addWidget(productQtyPLC);


        verticalLayout_4->addLayout(horizontalLayout_27);


        verticalLayout_3->addWidget(groupBox_4);

        horizontalLayout_28 = new QHBoxLayout();
        horizontalLayout_28->setSpacing(6);
        horizontalLayout_28->setObjectName(QStringLiteral("horizontalLayout_28"));
        label_28 = new QLabel(groupBox_2);
        label_28->setObjectName(QStringLiteral("label_28"));

        horizontalLayout_28->addWidget(label_28);

        magicEndPLC = new QLineEdit(groupBox_2);
        magicEndPLC->setObjectName(QStringLiteral("magicEndPLC"));
        magicEndPLC->setInputMethodHints(Qt::ImhDigitsOnly);
        magicEndPLC->setReadOnly(true);

        horizontalLayout_28->addWidget(magicEndPLC);


        verticalLayout_3->addLayout(horizontalLayout_28);

        line_4 = new QFrame(groupBox_2);
        line_4->setObjectName(QStringLiteral("line_4"));
        line_4->setFrameShape(QFrame::HLine);
        line_4->setFrameShadow(QFrame::Sunken);

        verticalLayout_3->addWidget(line_4);

        plc2erpDataBlockContent = new QPlainTextEdit(groupBox_2);
        plc2erpDataBlockContent->setObjectName(QStringLiteral("plc2erpDataBlockContent"));
        plc2erpDataBlockContent->setReadOnly(true);

        verticalLayout_3->addWidget(plc2erpDataBlockContent);

        showAlarmsPb = new QPushButton(groupBox_2);
        showAlarmsPb->setObjectName(QStringLiteral("showAlarmsPb"));

        verticalLayout_3->addWidget(showAlarmsPb);


        horizontalLayout_29->addWidget(groupBox_2);


        verticalLayout_5->addLayout(horizontalLayout_29);

        infoBarContent = new QPlainTextEdit(centralWidget);
        infoBarContent->setObjectName(QStringLiteral("infoBarContent"));

        verticalLayout_5->addWidget(infoBarContent);

        showSettingsPb = new QPushButton(centralWidget);
        showSettingsPb->setObjectName(QStringLiteral("showSettingsPb"));

        verticalLayout_5->addWidget(showSettingsPb);

        disconnectPb = new QPushButton(centralWidget);
        disconnectPb->setObjectName(QStringLiteral("disconnectPb"));

        verticalLayout_5->addWidget(disconnectPb);

        Snap7projectClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(Snap7projectClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1804, 21));
        Snap7projectClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(Snap7projectClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        Snap7projectClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(Snap7projectClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        Snap7projectClass->setStatusBar(statusBar);

        retranslateUi(Snap7projectClass);
        QObject::connect(connectToPlcPb, SIGNAL(clicked()), Snap7projectClass, SLOT(slotConnectToPLC()));
        QObject::connect(runClientPb, SIGNAL(clicked()), Snap7projectClass, SLOT(slotRunClient()));
        QObject::connect(showSettingsPb, SIGNAL(clicked()), Snap7projectClass, SLOT(slotSettings()));
        QObject::connect(disconnectPb, SIGNAL(clicked()), Snap7projectClass, SLOT(slotDisconnectPLC()));
        QObject::connect(enableManualEditMagicStartERP, SIGNAL(toggled(bool)), Snap7projectClass, SLOT(slotToggleEnableManualEditMagicStartERP(bool)));
        QObject::connect(enableManualEditMagicEndERP, SIGNAL(toggled(bool)), Snap7projectClass, SLOT(slotToggleEnableManualEditMagicStopERP(bool)));
        QObject::connect(enableManualEditLiveCounterERP, SIGNAL(toggled(bool)), Snap7projectClass, SLOT(slotToggleEnableManualEditLiveCounterERP(bool)));
        QObject::connect(enableManualEditErrorIDERP, SIGNAL(toggled(bool)), Snap7projectClass, SLOT(slotToggleEnableManualEditErrorIDERP(bool)));
        QObject::connect(showAlarmsPb, SIGNAL(clicked()), Snap7projectClass, SLOT(slotAlarms()));

        QMetaObject::connectSlotsByName(Snap7projectClass);
    } // setupUi

    void retranslateUi(QMainWindow *Snap7projectClass)
    {
        Snap7projectClass->setWindowTitle(QApplication::translate("Snap7projectClass", "ERP2PLC / PLC2ERP Simulation", Q_NULLPTR));
        connectToPlcPb->setText(QApplication::translate("Snap7projectClass", "Connect to PLC", Q_NULLPTR));
        runClientPb->setText(QApplication::translate("Snap7projectClass", "Run client", Q_NULLPTR));
        groupBox->setTitle(QApplication::translate("Snap7projectClass", "ERP => PLC ( PC SYSTEM - MACHINE )", Q_NULLPTR));
        label->setText(QApplication::translate("Snap7projectClass", "MAGIC NO DB START:", Q_NULLPTR));
        enableManualEditMagicStartERP->setText(QString());
        label_3->setText(QApplication::translate("Snap7projectClass", "LIVE COUNTER :", Q_NULLPTR));
        enableManualEditLiveCounterERP->setText(QString());
        label_4->setText(QApplication::translate("Snap7projectClass", "ERP OK:", Q_NULLPTR));
        toggleOkStatusERP->setText(QApplication::translate("Snap7projectClass", "Toggle", Q_NULLPTR));
        label_5->setText(QApplication::translate("Snap7projectClass", "ERP ERROR:", Q_NULLPTR));
        toggleErrorSatusERP->setText(QApplication::translate("Snap7projectClass", "Toggle", Q_NULLPTR));
        label_6->setText(QApplication::translate("Snap7projectClass", "ERP ERROR ID :", Q_NULLPTR));
        enableManualEditErrorIDERP->setText(QString());
        label_7->setText(QApplication::translate("Snap7projectClass", "Request new product:", Q_NULLPTR));
        submitNewProductERP->setText(QApplication::translate("Snap7projectClass", "Submit", Q_NULLPTR));
        label_8->setText(QApplication::translate("Snap7projectClass", "Request start cycle:", Q_NULLPTR));
        submitRequestStartCycleERP->setText(QApplication::translate("Snap7projectClass", "Submit", Q_NULLPTR));
        label_9->setText(QApplication::translate("Snap7projectClass", "Request pause cycle:", Q_NULLPTR));
        submitRequestPauseCycleERP->setText(QApplication::translate("Snap7projectClass", "Submit", Q_NULLPTR));
        label_10->setText(QApplication::translate("Snap7projectClass", "Request stop cycle:", Q_NULLPTR));
        submitRequestStopCycleERP->setText(QApplication::translate("Snap7projectClass", "Submit", Q_NULLPTR));
        label_11->setText(QApplication::translate("Snap7projectClass", "Request error reset:", Q_NULLPTR));
        submitRequestErrorResetERP->setText(QApplication::translate("Snap7projectClass", "Submit", Q_NULLPTR));
        groupBox_3->setTitle(QString());
        label_12->setText(QApplication::translate("Snap7projectClass", "Product stored:", Q_NULLPTR));
        submitProductStoredERP->setText(QApplication::translate("Snap7projectClass", "Submit", Q_NULLPTR));
        label_13->setText(QApplication::translate("Snap7projectClass", "Product type:", Q_NULLPTR));
        label_14->setText(QApplication::translate("Snap7projectClass", "Product qty:", Q_NULLPTR));
        label_2->setText(QApplication::translate("Snap7projectClass", "MAGIC NO DB END :", Q_NULLPTR));
        enableManualEditMagicEndERP->setText(QString());
        groupBox_2->setTitle(QApplication::translate("Snap7projectClass", "PLC => ERP ( MACHINE -> PC SYSTEM )", Q_NULLPTR));
        label_15->setText(QApplication::translate("Snap7projectClass", "MAGIC NO DB START:", Q_NULLPTR));
        label_16->setText(QApplication::translate("Snap7projectClass", "LIVE COUNTER :", Q_NULLPTR));
        label_17->setText(QApplication::translate("Snap7projectClass", "READY:", Q_NULLPTR));
        readyPLC->setText(QString());
        label_18->setText(QApplication::translate("Snap7projectClass", "ERROR:", Q_NULLPTR));
        errorPLC->setText(QString());
        label_30->setText(QApplication::translate("Snap7projectClass", "CYCLE RUN:", Q_NULLPTR));
        cycleRunPLC->setText(QString());
        label_31->setText(QApplication::translate("Snap7projectClass", "CYCLE PAUSE:", Q_NULLPTR));
        cyclePausePLC->setText(QString());
        label_32->setText(QApplication::translate("Snap7projectClass", "CYCLE STOP:", Q_NULLPTR));
        cycleStopPLC->setText(QString());
        label_33->setText(QApplication::translate("Snap7projectClass", "CYCLE STEP:", Q_NULLPTR));
        label_20->setText(QApplication::translate("Snap7projectClass", "Product manufacturing:", Q_NULLPTR));
        productManufacturingPLC->setText(QString());
        label_21->setText(QApplication::translate("Snap7projectClass", "Product ready:", Q_NULLPTR));
        productReadyPLC->setText(QString());
        label_22->setText(QApplication::translate("Snap7projectClass", "Product error :", Q_NULLPTR));
        productErrorPLC->setText(QString());
        label_23->setText(QApplication::translate("Snap7projectClass", "Product accepted:", Q_NULLPTR));
        productAcceptedPLC->setText(QString());
        label_24->setText(QApplication::translate("Snap7projectClass", "Product invalid:", Q_NULLPTR));
        productInvalidPLC->setText(QString());
        label_25->setText(QApplication::translate("Snap7projectClass", "Product loaded:", Q_NULLPTR));
        productLoadedPLC->setText(QString());
        groupBox_4->setTitle(QString());
        label_26->setText(QApplication::translate("Snap7projectClass", "Product type:", Q_NULLPTR));
        label_27->setText(QApplication::translate("Snap7projectClass", "Product qty:", Q_NULLPTR));
        label_28->setText(QApplication::translate("Snap7projectClass", "MAGIC NO DB END :", Q_NULLPTR));
        showAlarmsPb->setText(QApplication::translate("Snap7projectClass", "ALARMS", Q_NULLPTR));
        showSettingsPb->setText(QApplication::translate("Snap7projectClass", "Settings", Q_NULLPTR));
        disconnectPb->setText(QApplication::translate("Snap7projectClass", "Disconnect", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class Snap7projectClass: public Ui_Snap7projectClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SNAP7PROJECT_H
