#include "Snap7plc.h"
#include "snap7.h"


Snap7plc::Snap7plc() :
   dbread_no(0), dbread_start(0), dbread_buffer(nullptr),
   dbwrite_no(0), dbwrite_start(0), dbwrite_buffer(nullptr),
   ip_address(nullptr)
{

   client = new TS7Client();
}


Snap7plc::~Snap7plc()
{
   delete ip_address;
   delete client;
}

void Snap7plc::SetIpAddress(const char* ipAddress)
{
   int len = 0;

   if (nullptr != ip_address)
      delete ip_address;

   if (nullptr != ipAddress) {

      if ((len = strlen(ipAddress)) > 0) {
         ip_address = new char[len + 1];
         memset(ip_address, 0, len + 1);
         memcpy(ip_address, ipAddress, len);
      }
   }
}

void Snap7plc::SetDBReadArea(int DBNumber, int Start, int Size)
{
   dbread_no = DBNumber;
   dbread_start = Start;
   dbread_size = Size;
   dbread_buffer = new uint8_t[Size];

}

void Snap7plc::SetDBWriteArea(int DBNumber, int Start, int Size)
{
   dbwrite_no = DBNumber;
   dbwrite_start = Start;
   dbwrite_size = Size;
   dbwrite_buffer = new uint8_t[Size];
}

int  Snap7plc::Connect()
{
   int result{ -1 };

   try
   {
      if(nullptr!= ip_address)
         result = client->ConnectTo(ip_address,0,0);
   }
   catch (const std::exception&)
   {
      /* Just for nothing! */
   }

   return result;
}

bool Snap7plc::isConnected()
{
   bool result{ false };
   if (nullptr != client)
      result = client->Connected();

   return result;
}

int  Snap7plc::Disconnect() 
{
   int result{ -1 };

   if (isConnected())
      result = client->Disconnect();

   return result;
}

int  Snap7plc::DBRead()
{
   int result{ -1 };
   if (nullptr != dbread_buffer && nullptr != client) {

      if(client->Connected())
         result = client->DBRead(dbread_no, dbread_start, dbread_size, dbread_buffer);
   }

   return result;
}

int  Snap7plc::DBWrite()
{
   int result{ -1 };
   if (nullptr != dbwrite_buffer && nullptr != client) {

      if (client->Connected())
         result = client->DBWrite(dbwrite_no, dbwrite_start, dbwrite_size, dbwrite_buffer);
   }

   return result;
}

void Snap7plc::DBGetData(void *dbData, int dataSize)
{
   if (dbread_size <= dataSize) {
      if (nullptr != dbData && nullptr != dbread_buffer)
         memcpy(dbData, dbread_buffer, dataSize);
   }
}

void  Snap7plc::DBSetData(void *dbData, int dataSize)
{
   if (dbwrite_size >= dataSize) {
      if(nullptr!= dbwrite_buffer)
         memcpy(dbwrite_buffer, dbData, dataSize);
   }
}
